# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 1.0.5
#   kernelspec:
#     display_name: ocr_insurance
#     language: python
#     name: ocr_insurance
# ---

# # get labels from excel file

# **Description:** Get "cleaned" label data from the original dummy_data excel to a .csv file as well as a version in pickle dictionary form.
#
# What got cleaned?
# 1. Import the data all in "string", so that number like "0123" will preverse the digit "0" at the beginning of the number
# 2. Only keep columns containing information from the 19 relevant fields
# 3. Rename the fields in English only form
# 4. Normalise Japanese text
#
# Extra:
# 5. Compute missing rate for each field
#
# Output files:
# 1. cleaned_label.csv 
# 2. label_dict.pkl
# 3. missing_rates.csv
#
# They are saved in Sharepoint `02_Project/01_ocr_insurance_card/AI_Lab/Data/`

# %load_ext autoreload
# %autoreload 2

import pandas as pd
from src.config import PrjInsuranceConf
from src.ocr_base.text_processing.normlise_text import Normaliser
import pickle  # library to save python dictionaries into a file that can be loaded again
from jupyter_helpers.namespace import NeatNamespace

# **File required**: 
#
# *Sharepoint Location:* `Documents/02_Project/01_ocr_insurance_card/AI_Lab/Data/label/dummy_data_remove_empty_cell.xlsx`
#
# *Description:* A edited version of the inital excel file, `dummy data.xlsx`, uploaded by Chang-san.

# *Changes made in this version:*
#
# 1. Remove non-application information, which cells covered in black colour in the inital file, yet still contain text information
# 2. Remove items that are not shown on the cards, but not covered in black
# 3. Change the Date in English form to Japanese for images 562-578

# **Variables Needed Setting up:**

# After downloading the edited file on Sharepoint, update the path to that file for variable `label_xlsx_path` below

label_xlsx_path = PrjInsuranceConf.remove_empty_cell_label_xlsx_path
label_xlsx_path

# directory to save the output
out_path = PrjInsuranceConf.prj_cloud_dir_path + "/AI_Lab/Data/"
print(out_path)

# ## Load and clean the data

# load data
label_xlsx = pd.read_excel(label_xlsx_path, index_col = 1, dtype=str)
label_xlsx[562:578]

# check datatype of column contains Insurance Number
# to make sure its type is string 
# so that any value that starts with "0" would not be dismissed
# i.e: we would expect the Insurance number of the first image would be "06260012", not "6260012"
label_xlsx.iloc[:3,22]

# get information from the first 20 columns as label
label_df = label_xlsx.iloc[:,0:20]
label_df = pd.DataFrame(label_df)
# add the 22nd column for Insurance Number info
col_InsuranceNumber = list(label_xlsx.iloc[:,22])
label_df["InsuranceNumber"] = col_InsuranceNumber
print(label_df.shape)
label_df.iloc[:3,:]
# delete the irrelevant column named "保険者番号"
label_df = label_df.drop(columns="保険者番号")
label_df.head(3)

# +
# rename the columns
col_name = ["InitialImage", "Dependent", "Symbol", "Number", "Name", "NameKatakana", "Sex", 
            "Birthday", "AcquisitionDate", "IssueDate", "InsuredPersonName", "ExpireDate",
            "Address", "WorkplaceName", "WorkplaceAddress", "WorkplacePhoneNo", "InsurerName",
           "IssuerAddress", "IssuerPhoneNo", "InsuranceNumber"]

print(len(col_name))
label_df.columns = col_name
label_df.iloc[:3,:]
# -

# rename index column
label_df.index.names = ["ImageName"]
label_df.iloc[:3,:]

# normalise japanese characters 
for col in range(label_df.shape[1]):
    for text in label_df.iloc[:,col]:
        if pd.notnull(text):
            text_idx = list(label_df.iloc[:,col]).index(text)
            ori_text = str(text)
    #         print("ori_text:", text)
            norm_text = Normaliser.normalize_neologd(ori_text)
    #         print("norm_text:", norm_text)
            label_df.iloc[text_idx,col] = norm_text

label_df.head(3)

# rename index value into string
# and add "0" in front of image name that is smaller than 10
# just to make the index colies with the naming in the dataset
# i.e: we want to have ImageName for the first image is "01", not "1" or 1
for value in label_df.index:
#     print(value)
    if value < 10:
        value_str = "0" + str(value)
    else:
        value_str = str(value)
    label_df.rename(index={value: value_str}, inplace=True)

label_df.head()

label_df.info()

# inspect the missing rate for each field
missing_rates = 1 - label_df.count()/len(label_df)
missing_rates

# re-order the columns so that the fields with no missing values (i.e: must-have fields) will appear first
missing_rates = pd.DataFrame(missing_rates, columns=["missing_rate"])
missing_rates = missing_rates.sort_values(by=['missing_rate'])
missing_rates = missing_rates.round(2)
# remove row "InitialImage"
missing_rates = missing_rates.drop(index="InitialImage")
missing_rates

# compute "must-have" list
must_have = missing_rates[(missing_rates.missing_rate == 0)]
must_have_list = must_have.index.values.tolist()
must_have_list

# +
# # re-index the label file based on the missing rate
# re_index_list = missing_rates.index.values.tolist()
# label_df.reindex(re_index_list)
# -

# ## save the processed dataframe

# to_csv
label_df.to_csv(out_path+"/label/cleaned_label.csv", index = "ImageName", header=True)
missing_rates.to_csv(out_path+"/missing_rates.csv", header=True)

# to_python_dictionary
label_dict = label_df.to_dict("index")
NeatNamespace(label_dict["562"])

# make and save the dictionary as pickle file that can be reloaded later on
# path to labled dict
label_dict_dir = out_path + "label_dict.pkl"
print(label_dict_dir)
f = open(label_dict_dir,"wb")
pickle.dump(label_dict,f)
f.close()

# label dictionary
label_dict_1 = pickle.load(open(label_dict_dir, 'rb'))

label_dict_1["562"]

import math
a = float("NaN")
a

if np.isnan(a):
    print("T")

b = label_dict_1["01"]["WorkplaceName"]
b

np.isnan(b)

a == b

c = "06260012"
c

pd.isna(c)

pd.isna(b)

pd.isna(a)


