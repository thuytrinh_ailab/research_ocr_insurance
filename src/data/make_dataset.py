# -*- coding: utf-8 -*-
# import logging
# from pathlib import Path
# from dotenv import find_dotenv, load_dotenv

import pandas as pd
from src.config import label_xlsx_path, processed_data_path
from src.ocr_base.text_processing.normlise_text import Normaliser
import pickle


def make_label():
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    # logger = logging.getLogger(__name__)
    # logger.info('making final data set from raw data')

    # label_csv = pd.read_csv(label_csv_path, index_col=1, dtype=str)
    label_xlsx = pd.read_excel(label_xlsx_path, index_col=1, dtype=str)

    # get information from the first 20 columns as label
    label_df = label_xlsx.iloc[:, 0:20]
    # add the 22nd column for Insurance Number info
    insuranceNumb_values = list(label_xlsx.iloc[:, 22])
    label_df.loc[:, "InsuranceNumber"] = insuranceNumb_values
    label_df = pd.DataFrame(label_df)

    # rename the columns
    col_name = ["InitialImage", "Dependent", "Symbol", "Number", "Name", "NameKatakana", "Sex",
                "Birthday", "AcquisitionDate", "IssueDate", "InsuredPersonName", "ExpireDate",
                "Address", "WorkplaceName", "WorkplaceAddress", "WorkplacePhoneNo", "保険者番号", "InsurerName",
                "IssuerAddress", "IssuerPhoneNo", "InsuranceNumber"]

    label_df.columns = col_name

    # rename index column
    label_df.index.names = ["ImageName"]

    # normalise japanese characters
    for col in range(label_df.shape[1]):
        for text in label_df.iloc[:, col]:
            text_idx = list(label_df.iloc[:, col]).index(text)
            ori_text = str(text)
            norm_text = Normaliser.normalize_neologd(ori_text)
            label_df.iloc[text_idx, col] = norm_text

    # rename index value into string
    for value in label_df.index:
        #     print(value)
        if value < 10:
            value_str = "0" + str(value)
        else:
            value_str = str(value)
        label_df.rename(index={value: value_str}, inplace=True)

    # save the processed dataframe
    # to_csv
    label_df.to_csv(processed_data_path + "/label.csv", index="ImageName", header=True)
    # to_python_dictionary
    label_dict = label_df.to_dict("index")
    # make and save the dictionary as pickle file that can be reloaded later on
    f = open(processed_data_path + "/label_dict.pkl", "wb")
    pickle.dump(label_dict, f)
    f.close()


# def get_gcv_response():



if __name__ == '__main__':
    # log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    # logging.basicConfig(level=logging.INFO, format=log_fmt)
    #
    # # not used in this stub but often useful for finding various files
    # project_dir = Path(__file__).resolve().parents[2]
    #
    # # find .env automagically by walking up directories until it's found, then
    # # load up the .env entries as environment variables
    # load_dotenv(find_dotenv())

    make_label()
