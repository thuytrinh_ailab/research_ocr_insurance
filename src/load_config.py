import os
from dotenv import load_dotenv, find_dotenv

# find .env automagically by walking up directories until it's found
dotenv_path = find_dotenv()

# load up the entries as environment variables
load_dotenv(dotenv_path)

# assign important information
gcv_key_jsonfile = os.environ.get("gcv_key_jsonfilepath")
gcv_language = os.environ.get("gcv_language")

