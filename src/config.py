# google computer vision API
class GCV:
    gcv_key_jsonfilepath = "/Users/thuyttt/Documents/Documents - VN0128/AILab/02_Project/OCR/" \
                           "MTI OCR feeder-credential.json"
    gcv_language = ["ja", "en"]


class PrjInsuranceConf:
    # local config
    work_dir_path = "/Users/thuyttt/Projects/OCR/ailabo_ocr_insurance"

    # personal private cloud config
    private_cloud_dir_path = "/Users/thuyttt/OneDrive - MTI/AILab/06_Data/ocr_insurance_cards"

    # raw data
    raw_img_path = private_cloud_dir_path + "/raw/images"
    raw_label_file_path = private_cloud_dir_path + "/raw/label/dummy data.xlsx"

    # project cloud config (SharePoint location to for team shared documents)
    # all relevant outputs from local machine will be stored here
    prj_cloud_dir_path = "/Users/thuyttt/MTI/MTI Technology AI Lab - Documents/02_Project/01_ocr_insurance_card"

    # interim data
    remove_empty_cell_label_xlsx_path = prj_cloud_dir_path + "/AI_Lab/Data/label/dummy_data_remove_empty_cell.xlsx"
    gcv_response_path = prj_cloud_dir_path + "/AI_Lab/Data/gcv_response/gcv_response.pkl"
