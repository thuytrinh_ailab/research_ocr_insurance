## load packages
from src.ocr_insurance_card.pipeline.load_packages import *
from src.ocr_base.text_processing.match_inline_texts import MatchLine
from src.ocr_insurance_card.pipeline.extract_info import *

## load data
from src.ocr_insurance_card.pipeline.load_data import *

# test image
img_name = "01"
img_extension = ".jpg"
img_path = image_dir + "/" + img_name + img_extension
img = cv2.imread(img_path, 1)
# Display.display_img_nb(img)


raw_text = gcv_text_dict["01"]
print("raw_text:", raw_text)

reformed_text = PR.gcv_text_reform(text=raw_text)
print("reformed_text", reformed_text)

# keywords = hint_dict["Dependent"]["keyword"]
# data_type = hint_dict["Dependent"]["type"]
#
# print("keywords:", keywords)
# print("data_type:", data_type)
#
# print(PR.data_type_to_regex(data_type=data_type))
# print(PR.regex_gen(keywords=keywords, output_type=data_type))
#
# pred = PR.extract_info_regex(text=reformed_text)
# pprint.pprint(pred)


# ### run for multiple images/keys
# pred_multi = PR.extract_info_multi(input_dict=gcv_text_dict)
# # pprint.pprint(pred_multi)
#
#
#
# ### compute accuracy
# accuracy = Metric.metric(predict=pred_multi, target=label_dict)
# pprint.pprint(accuracy, depth=3, width=100)