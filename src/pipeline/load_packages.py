## packages
from src.config import work_dir_path
from src.ocr_base.text_processing.normlise_text import Normaliser
from src.ocr_base.display.display import Display
from src.ocr_base.accuracy.metric import Metric
from src.ocr_insurance_card.text_processing.regex import PostprocessRegex as PR

import pickle  # library to save python dictionaries into a file that can be loaded again
import pandas as pd
import re
import cv2
