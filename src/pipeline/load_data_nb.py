## packages
from src.config import work_dir_path
from src.ocr_base.text_processing.normlise_text import Normaliser
from src.ocr_base.display.display import Display
from src.ocr_base.accuracy.metric import Metric
import pickle  # library to save python dictionaries into a file that can be loaded again
from jupyter_helpers.namespace import NeatNamespace # package to summarise dictionaries into nice table view, syntax NeatNamespace(theDictionary)
import IPython
import pandas as pd
import re
from src.ocr_insurance_card.text_processing.regex import PostprocessRegex as PR