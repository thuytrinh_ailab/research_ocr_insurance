
from src.ocr_insurance_card.pipeline.load_data import *


def extract_info_multi(input_dict, reform_text_method, extract_info_method, key="all", fields=interested_fields):
    """
    extract information for multiple files
    :param input_dict: dictionary contains all input texts returned by gcv, with @key: image name without extension
    :param reform_text_method: method used to reform the raw text return by gcv
    :param extract_info_method: method used to extract interested information from the input text
    :param key: name of images
    :param fields: name of fields that need extracting
    :return: a dictionary contains all predicting output
    """

    if key == "all":
        interested_keys = list(input_dict.keys())
    else:
        interested_keys = key

    pred = {k: {} for k in interested_keys}
    for key in interested_keys:
        reformed_text = reform_text_method(input_dict[key])
        pred[key] = extract_info_method(text=reformed_text, hint_dict=hint_dict, fields=fields)

    return pred

