# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 1.0.5
#   kernelspec:
#     display_name: ocr_insurance
#     language: python
#     name: ocr_insurance
# ---

from src.config import work_dir_path
import pickle  # library to save python dictionaries into a file that can be loaded again

# + {"endofcell": "--"}
# ## load data
# ### neccessary files
# path to labled dict
label_dict_dir = "/Users/thuyttt/Projects/OCR/ailabo_ocr_insurance/data/processed/label/label_dict.pkl"

# path to gcv response dict
gcv_dict_dir = "/Users/thuyttt/Projects/OCR/ailabo_ocr_insurance/data/processed/gcv_response/gcv_response.pkl"
gcv_text_dict_dir = "/Users/thuyttt/Projects/OCR/ailabo_ocr_insurance/data/processed/gcv_response/gcv_text_dict.pkl"
# -
# --

# +
# ### load dictionaries

# label dictionary
print("\n Loading label dictionary from the file \n {}". format(label_dict_dir))
try:
    label_dict = pickle.load(open(label_dict_dir, 'rb'))
    print("\n Successully loaded!")
except:
    print("Unable to load the file")

# +
# gcv response dictionary
gcv_res_dict = pickle.load(open(gcv_dict_dir, 'rb'))

# dictionary containing all of the longest text returned by GCV
gcv_text_dict = {k: gcv_res_dict[k]['final_text'] for k in list(gcv_res_dict.keys())}

print("\n Loading google API response dictionary from the files \n {} \n {}". format(gcv_dict_dir, gcv_text_dict_dir))
try:
    gcv_res_dict = pickle.load(open(gcv_dict_dir, 'rb'))
    gcv_text_dict = {k: gcv_res_dict[k]['final_text'] for k in list(gcv_res_dict.keys())}
    print("\n Successully loaded!")
except:
    print("Unable to load the file")

# +
# label_dict["01"]

# +
# gcv_res_dict["01"]
# -


