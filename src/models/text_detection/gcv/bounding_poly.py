# import the necessary packages
import numpy as np
import cv2


class BoundingPoly:

    @staticmethod
    def convert_to_array(bp_raw):
        """
        convert raw bounding poly from string into np array
        :param bp_raw:
        :return: bp_array
        """
        if isinstance(bp_raw, list):
            bp_array = []
            for value in bp_raw:
                tmp_array = np.array(eval(value), dtype = "float32")
                bp_array.append(tmp_array)
        else:
            bp_array = np.array(eval(bp_raw), dtype = "float32")

        return bp_array

    @staticmethod
    def order_points(pts):
        """
        initialise a list of coordinates that will be ordered
        such that the first entry in the list is the top-left,
        the second entry is the top-right, the third is the
        bottom-right, and the fourth is the bottom-left

        :param pts: bounding poly returned by GCV
        :return: rect: 4 points in rectangular form: (tl, tr, br, bl)
        """
        rect = np.zeros((4, 2), dtype = "float32")

        # the top-left point will have the smallest sum, whereas
        # the bottom-right point will have the largest sum
        s = pts.sum(axis=1)
        rect[0] = pts[np.argmin(s)]
        rect[2] = pts[np.argmax(s)]

        # now, compute the difference between the points, the
        # top-right point will have the smallest difference,
        # whereas the bottom-left will have the largest difference
        diff = np.diff(pts, axis = 1)
        print(diff)
        rect[1] = pts[np.argmin(diff)]
        rect[3] = pts[np.argmax(diff)]

        # # # now, compare the other 2 points
        # # # which of those have smaller x will be at bottom-left,
        # # # the one with bigger x will be at top-right
        # # # remove the tl and br from pts
        # tr_bl_idx = list(range(4))
        # del tr_bl_idx[np.argmax(s)]
        # del tr_bl_idx[np.argmin(s)]
        # x1 = list(pts[tr_bl_idx[0]])[0]
        # x2 = list(pts[tr_bl_idx[1]])[0]
        #
        # if x1 < x2:
        #     rect[3] = pts[tr_bl_idx[0]]
        #     rect[1] = pts[tr_bl_idx[1]]
        # else:
        #     rect[1] = pts[tr_bl_idx[0]]
        #     rect[3] = pts[tr_bl_idx[1]]

        # return the ordered coordinates
        return rect

    @staticmethod
    def rect_area(pts):
        """
        compute area of the rectangular form by the 4 points in the input pts

        :param pts:
        :return:
        area of the rect
        """
        rect = BoundingPoly.order_points(pts)
        (tl, tr, br, bl) = rect

        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        # compute the area of the form rectangular
        area = maxWidth * maxHeight

        return area
 

    @staticmethod
    def four_point_transform(image, pts):
        """
        obtain a consistent order of the points and unpack them
        individually

        :param image:
        :param pts:
        :return:
        """

        rect = BoundingPoly.order_points(pts)
        (tl, tr, br, bl) = rect

        # compute the width of the new image, which will be the
        # maximum distance between bottom-right and bottom-left
        # x-coordiates or the top-right and top-left x-coordinates
        widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
        widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
        maxWidth = max(int(widthA), int(widthB))

        # compute the height of the new image, which will be the
        # maximum distance between the top-right and bottom-right
        # y-coordinates or the top-left and bottom-left y-coordinates
        heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
        heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
        maxHeight = max(int(heightA), int(heightB))

        # now that we have the dimensions of the new image, construct
        # the set of destination points to obtain a "birds eye view",
        # (i.e. top-down view) of the image, again specifying points
        # in the top-left, top-right, bottom-right, and bottom-left
        # order
        dst = np.array([
            [0, 0],
            [maxWidth - 1, 0],
            [maxWidth - 1, maxHeight - 1],
            [0, maxHeight - 1]], dtype = "float32")

        # compute the perspective transform matrix and then apply it
        M = cv2.getPerspectiveTransform(rect, dst)
        warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

        # return the warped image
        return warped