# google text detection
import io
import os

from google.cloud import vision
from src.config import gcv_key_jsonfilepath, gcv_language

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = gcv_key_jsonfilepath

vision_client = vision.ImageAnnotatorClient()


class TextDetector:

    def detect_text(path):

        """Detects text in the file using google API cloud vision.
           Note: here we also give the server language hint: Japanese and English
        """
        from google.cloud import vision
        client = vision.ImageAnnotatorClient()

        with io.open(path, 'rb') as image_file:
            content = image_file.read()

        image = vision.types.Image(content=content)

        # Language hint codes for OCR: Japanese "ja" and English "en".
        image_context = vision.types.ImageContext(
            language_hints=gcv_language)

        response = client.document_text_detection(image=image,
                                                  image_context=image_context)

        texts = response.text_annotations

        text_res = []
        for text in texts:
            text_res.append(text.description)

        return (text_res)