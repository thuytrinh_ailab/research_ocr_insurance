# google text detection
import io
import os
from src.config import gcv_key_jsonfilepath, gcv_language
from google.cloud import vision

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = gcv_key_jsonfilepath
client = vision.ImageAnnotatorClient()

# class FeatureType(Enum):
#     PAGE = 1
#     BLOCK = 2
#     PARA = 3
#     WORD = 4
#     SYMBOL = 5


class Detector:
    def __init__(self, image_path):
        with io.open(image_path, 'rb') as image_file:
            self.content = image_file.read()
        self.image = vision.types.Image(content=self.content)
        # Language hint codes for OCR
        self.image_context = vision.types.ImageContext(
            language_hints=gcv_language)
        self.response = client.document_text_detection(image=self.image,
                                                       image_context=self.image_context)


    def text(self):
        """
        Detects text in the file using google API cloud vision.
        Note: here we also give the server language hint: Japanese and English
        """
        text_annotations = self.response.text_annotations

        detected_texts = []
        bounding_poly = []
        for text in text_annotations:
            # print(text, "\n")
            detected_texts.append(text.description)

            vertices = (['({:d},{:d})'.format(vertex.x, vertex.y)
                         for vertex in text.bounding_poly.vertices])
            four_points = '[{}]'.format(','.join(vertices))
            bounding_poly.append(four_points)

        return text_annotations, detected_texts, bounding_poly


    def document(self):

        full_document = self.response.full_text_annotation
        print("\n full doc: \n", full_document)

        # Collect specified feature bounds by enumerating all document features
        for page in self.response.full_text_annotation.pages:
            # block_type = []
            for block in page.blocks:
                # print('\nBlock confidence: {}\n'.format(block.confidence))

                # block_type = block_type.append(block.)

                block_text = ''
                for paragraph in block.paragraphs:
                    # print('Paragraph confidence: {}'.format(
                    #     paragraph.confidence))

                    paragraph_text = ''
                    for word in paragraph.words:
                        word_text = ''.join([
                            symbol.text for symbol in word.symbols
                        ])
                        # print('Word text: {} (confidence: {})'.format(
                        #     word_text, word.confidence))
                        paragraph_text = paragraph_text.join(word_text)

                        # for symbol in word.symbols:
                        #     print('\tSymbol: {} (confidence: {})'.format(
                        #         symbol.text, symbol.confidence))

                    # print('Paragraph text: {}'.format(
                    #     paragraph_text))

                    block_text = block_text.join(paragraph_text)

                # print('Block text: {}'.format(
                #     block_text))

        return full_document, block_text, paragraph_text


