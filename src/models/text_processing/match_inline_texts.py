# import the necessary packages
import warnings
import numpy as np
from src.ocr_base.text_detection.gcv.bounding_poly import BoundingPoly as BP


class MatchLine:

    @staticmethod
    def overlap(two_bp):
        """
        check if any 2 bounding poly overlap each other

        :param two_bp:
        :return:
        """
        (tl1, tr1, br1, bl1) = two_bp[0]
        (tl2, tr2, br2, bl2) = two_bp[1]
        (ml1, mr1) = ((tl1 + bl1) / 2, (tr1 + br1) / 2)
        (ml2, mr2) = ((tl2 + bl2) / 2, (tr2 + br2) / 2)
        # compute area of the rectangular formed
        # by the 4 points that are the co-ordinators of the given 2 lines
        fpts_bottom = np.array([br1, bl1, br2, bl2])
        fpts_top = np.array([tl1, tr1, tl2, tr2])
        fpts_middel = np.array([ml1, mr1, ml1, mr2])
        area = BP.rect_area(fpts_bottom)
        # concat the 2 texts if the area is very small or close to 0
        error = 20
        # print('area', area)
        # if BoundingPoly.rect_area(fpts_bottom) < error \
        #         or BoundingPoly.rect_area(fpts_top) < error \
        #         or BoundingPoly.rect_area(fpts_middel) < error:
        if BP.rect_area(fpts_bottom) < error:
            # print("fpts\n", fpts_bottom)
            # print("ordered_fpts\n", BP.order_points(fpts_bottom))
            return True
        else:
            return False


    @staticmethod
    def merge_two_inline_texts(two_texts):
        text1 = two_texts[0]
        text2 = two_texts[1]
        # form joint text
        joint_text = text1 + " " + text2
        return joint_text

    @staticmethod
    def merge_two_inline_bp(two_bp):
        (tl1, tr1, br1, bl1) = two_bp[0]
        (tl2, tr2, br2, bl2) = two_bp[1]
        # form new bp
        joint_tl = [min(tl1[0], tl2[0]), min(tl1[1], tl2[1])]
        joint_br = [max(br1[0], br2[0]), max(br1[1], br2[1])]
        joint_tr = [max(tr1[0], tr2[0]), min(tr1[1], tr2[1])]
        joint_bl = [min(bl1[0], bl2[0]), max(bl1[1], bl2[1])]
        joint_bp = np.array([joint_tl, joint_tr, joint_br, joint_bl])
        return joint_bp

    @staticmethod
    def prepare_data_for_match(gcv_response):

        block_words = gcv_response["final_text"]
        element_words = gcv_response["full_text"][1:]
        element_words_bp = gcv_response["bouding_poly"][1:]

        # convert raw bp into array
        element_words_bp = BP.convert_to_array(element_words_bp)

        return block_words, element_words, element_words_bp


    @staticmethod
    def find_block_words_bp(gcv_response):

        block_words, element_words, element_words_bp = MatchLine.prepare_data_for_match(gcv_response)
        # separate block_words into vector of separate texts
        sep_texts = block_words.split("\n")
        sep_texts = [x for x in sep_texts if x != '']

        match_texts = []
        match_bp = []
        for value in sep_texts:
            test_text = value
            # print("test_text", test_text)
            # print("element_words", element_words)
            # find all the element strings that are part of the text
            element_texts = []
            element_bp = []
            match_idx = []
            for idx in range(len(element_words)):
                text = element_words[idx]
                # print("text", text)
                if text in test_text:
                    # print("matched")
                    element_texts.append(text)
                    element_bp.append(element_words_bp[idx])
                    match_idx.append(idx)
                    # remove the found text in element_words
                    test_text = test_text.replace(text, "", 1)

            # print("element_texts", element_texts)
            # print("element_bp", element_bp)
            # match the texts and find the joint bp
            for idx in range(len(element_texts) - 1):
                two_texts = element_texts[:2]
                two_bp = element_bp[:2]
                joint_text = MatchLine.merge_two_inline_texts(two_texts)
                joint_bp = MatchLine.merge_two_inline_bp(two_bp)
                # remove the 2 texts from the element_texts vectors
                del element_texts[:2]
                del element_bp[:2]
                # add the joint_text and joint_bp to element_bp vectors
                element_texts.insert(0, joint_text)
                element_bp.insert(0, joint_bp)

            # remove empty string
            if len(element_texts) > 0:
                match_texts.append(element_texts[0])
                match_bp.append(element_bp[0])
            # print("match_bp", match_bp)

            # print(test_text)
            # print("match_idx", match_idx)
            for idx in match_idx:
                element_words[idx] = ''
                element_words_bp[idx] = ''

            # element_words.remove('')
            # element_words_bp.remove('')
            element_words = [x for x in element_words if x != '']
            warnings.simplefilter(action='ignore', category=FutureWarning)
            element_words_bp = [x for x in element_words_bp if x != '']  # returns False, without Warning

        return match_texts, match_bp


    @staticmethod
    def two_bp_distance(two_bp):
        """
        check if any 2 bounding poly overlap each other

        :param two_bp:
        :return:
        """
        (tl1, tr1, br1, bl1) = two_bp[0]
        (tl2, tr2, br2, bl2) = two_bp[1]
        (ml1, mr1) = ((tl1 + bl1) / 2, (tr1 + br1) / 2)
        (ml2, mr2) = ((tl2 + bl2) / 2, (tr2 + br2) / 2)
        # compute the distance of the 2 bottom lines
        d_bottom = np.abs((br1[1] + bl1[1]) / 2 - (br2[1] + bl2[1]) / 2)
        # print("d", d)
        # compute the distance of the 2 middle lines
        d_middle = np.abs((mr1[1] + ml1[1]) / 2 - (mr2[1] + ml2[1]) / 2)

        # final distance
        # d = d_middle
        d = min(d_bottom, d_middle)

        return d


    @staticmethod
    def separate_by_distance(word_to_match, word_vector, bps, thres_d=20):

        d = []
        same_line_word = ''
        word_to_match_idx = word_vector.index(word_to_match)
        word_to_match_bp = bps[word_to_match_idx]

        same_line_word_idx = [word_to_match_idx]
        for idx in range(len(word_vector)):
            if idx != word_to_match_idx:
                two_bp = [word_to_match_bp, bps[idx]]
                tmp_d = MatchLine.two_bp_distance(two_bp)
                d.append(tmp_d)
                if tmp_d < thres_d:
                    same_line_word_idx.append(idx)

        for idx in same_line_word_idx:
            same_line_word = same_line_word + " " + word_vector[idx]
        # print(same_line_word)
        # print(d)

        return same_line_word_idx

    @staticmethod
    def match_inline(gcv_response):

        block_words, element_words, element_words_bp = MatchLine.prepare_data_for_match(gcv_response)

        # find bounding poly for the block of texts in the first and also
        # the longest element in text_annotation response return from gcv
        match_texts, match_bp = MatchLine.find_block_words_bp(gcv_response)

        same_line_word_idx = []
        for word in match_texts:
            tmp_same_line_word_idx = MatchLine.separate_by_distance(word_to_match=word,
                                                                    word_vector=match_texts,
                                                                    bps=match_bp)
            #     print("same_line_word_idx", tmp_same_line_word_idx)
            sorted_idx = sorted(tmp_same_line_word_idx)
            #     print("sorted_idx", sorted_idx)
            if sorted_idx not in same_line_word_idx:
                same_line_word_idx.append(sorted_idx)

        # remove duplicate groups that are subset of larger group
        more_than_one_idx = []
        for group in same_line_word_idx:
            if len(group) > 1:
                more_than_one_idx.append(group)

        # compare each of the group with the other group inside more_than_one_idx
        for target_group in more_than_one_idx:
            for compare_group in more_than_one_idx:
                if target_group != compare_group and len(compare_group) > len(target_group):
                    if set(target_group) <= set(compare_group) and target_group in same_line_word_idx:
                        # remove target_group from same_line_word_idx
                        same_line_word_idx.remove(target_group)

        # remove text that is empty
        sep_texts = block_words.split("\n")
        sep_texts = [x for x in sep_texts if x != '']
        same_line_texts = []
        for idx in same_line_word_idx:
            tmp_idx = idx
            if len(tmp_idx) > 1:
                tmp_text = ""
                for value in tmp_idx:
                    tmp_text = tmp_text + " " + sep_texts[value]
            else:
                tmp_text = sep_texts[tmp_idx[0]]
            same_line_texts.append(tmp_text)

        return same_line_texts



