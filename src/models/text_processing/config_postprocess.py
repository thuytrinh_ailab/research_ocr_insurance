# ### postprocessing
hint_dict = {"Dependent": {"keyword": ["本人", "家族", "被保険者"], "type": "dependent_word_form"},
             "Symbol": {"keyword": "記号", "type": "number"},
             "Number": {"keyword": "番号", "type": "number"},
             "Name": {"keyword": "氏名", "type": "person_name"},
             "NameKatakana": {"keyword": "フリガナ", "type": "word"},
             "Sex": {"keyword": "性別", "type": "sex"},
             "Birthday": {"keyword": "生年月日", "type": "date"},
             "AcquisitionDate": {"keyword": ["資格取得年月日", "認定年月日"], "type": "date"},
             "IssueDate": {"keyword": ["交付年月日", "交付"], "type": "date"},
             "InsuredPersonName": {"keyword": ["組合員氏名", "被保険者氏名", "世帯主氏名"], "type": "person_name"},
             "ExpireDate": {"keyword": "有効期限", "type": "date"},
             "Address": {"keyword": "住所", "type": "address"},
             "WorkplaceName": {"keyword": "事業所名称", "type": "word"},
             "WorkplaceAddress": {"keyword": "事業所所在地", "type": "address"},
             "WorkplacePhoneNo": {"keyword": "事業所電話番号", "type": "phone_no"},
             "保険者番号": {"keyword": "保険者番号", "type": "number"},
             "InsurerName": {"keyword": "保険者名称", "type": "word"},
             "IssuerAddress": {"keyword": "保険者所在地", "type": "address"},
             "IssuerPhoneNo": {"keyword": "保険者電話番号", "type": "phone_no"},
             "InsuranceNumber": {"keyword": "保険者番号", "type": "8_digit_number"}}

interested_fields = ["Dependent", "InsuranceNumber", "Symbol", "Number", "Name", "NameKatakana", "Sex",
                     "Birthday", "AcquisitionDate", "InsuredPersonName", "ExpireDate",
                     "InsurerName", "IssuerAddress", "IssuerPhoneNo",
                     "IssueDate", "Address", "WorkplaceName", "WorkplaceAddress", "WorkplacePhoneNo"]

data_type_regex = {"number": "\d{3,}",
                    "date": "\w{2}?\d{2}年\s?\d{1,2}月\d{1,2}日",
                    "phone_no": "\d{2,3}-\d{3,4}-\d{4}",
                    "person_name": "([^年月日\W\d]){1,3}\s([^年月日\W\d]){1,3}",
                    "sex": "男|女",
                    "address": "(\w*)(町\d+-\d+-\d+)",
                    "dependent_word_form": "((本人|家族)(\s*)(\(\s*被保険者\s*\))?)",
                    "8_digit_number": "\d{7,8}"
                    }

# ((\(\s*被保険者\s*\))|((本人|家族)(\s*)(\(\s*被保険者\s*\))?))
# ((本人|家族)?(\s*)(\(\s*被保険者\s*\)))