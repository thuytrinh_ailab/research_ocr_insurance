from src.config import work_dir_path
import re
import pandas as pd
from src.ocr_base.text_processing.normlise_text import Normaliser
from src.ocr_insurance_card.text_processing.config_postprocess import *


class PostprocessRegex:

    @staticmethod
    def gcv_text_reform(text):
        """
        reform the text returned by GCV
        2 steps: 1. normalise the text, 2. replace `\n` by space
        :param text: the first, longest text returned from GCV
        :return:
        text_concated: reformed text
        """

        # separate text by "\n" if needed
        if isinstance(text, list):
            texts_sep = text
        else:
            texts_sep = text.split("\n")

        text_concated = ""
        for string in texts_sep:
            # normalise the text
            idx = texts_sep.index(string)
            norm_text = Normaliser.unicode_normalize('０-９Ａ-Ｚａ-ｚ｡-ﾟ', string)
            texts_sep[idx] = norm_text

            # drop empty element
            if len(string) == 0:
                texts_sep.remove(string)
            else:
                text_concated = text_concated + " " + norm_text

        return text_concated

    @staticmethod
    def data_type_to_regex(data_type):
        """
        generate regex for different data type
        return any Alphanumeric characters as default value if none of the pre-defined type matched

        @para
        data_type: a string that defines type of data

        @retun
        corresponding regex
        """
        # switcher = {"number": "\d{3,}",
        #             "date": "\w{2}?\d{2}年\s?\d{1,2}月\d{1,2}日",
        #             "phone_no": "\d{2,3}-\d{3,4}-\d{4}",
        #             "person_name": "([^年月日\W\d]){1,3}\s([^年月日\W\d]){1,3}",
        #             "address": "(\w*)(町\d+-\d+-\d+)",
        #             "dependent_word_form": ""
        #             }
        switcher = data_type_regex

        return switcher.get(data_type, "([^年月日\W\d])*")


    @staticmethod
    def regex_gen(keywords, output_type, keyword_need=True, keyword_position="front"):
        """
        generating regex from keyword and output_type
        simple logic: keyword + space + regex of output_type

        @para
        keyword: can be a single word or a vectors of multiple keywords in string form
        output_type: a string that defines output type

        @return
        corresponding regex
        """
        output_type_regex = PostprocessRegex.data_type_to_regex(output_type)

        if isinstance(keywords, list):
            keyword = keywords[0]
            for key in keywords[1:]:
                keyword = keyword + "|" + key
        else:
            keyword = keywords

        if keyword_need:
            if keyword_position == "front":
                pattern = "(?P<keyword>" + keyword + ")" + "(\s*)" + "(?P<output>" + output_type_regex + ")"
            else:
                pattern = "(?P<output>" + output_type_regex + ")" + "(\s*)" + "(?P<keyword>" + keyword + ")"
        else:
            if keyword_position == "front":
                pattern = "(?P<keyword>" + keyword + ")?" + "(\s*)" + "(?P<output>" + output_type_regex + ")"
            else:
                pattern = "(?P<output>" + output_type_regex + ")" + "(\s*)" + "(?P<keyword>" + keyword + ")?"

        regex = re.compile("{}".format(pattern))
        return regex


    @staticmethod
    def extract_info_regex(text, hint_dict, fields):
        """
        extract information using regex of keyword and data type
        apply for one single image only

        :param text: a single string of text from which we need to extract information from
        :param hint_dict: dictionary containing 2 type of hints about the field: `keyword` and `type`
        :param fields: a vectors of name of the fields that need to extract information
        :return:
        pred: a dictionary contains all predicting output
        """
        pred = {f: float("NaN") for f in fields}
        # search the one time for regex with keyword and type
        found_texts = []

        def regex_search(regex, text, pred, field, text_inplace=True):
            x = re.search(regex, text)
            if x:
                # print("found match in 1st search!\n matched text:", text)
                # print("output:", x.group("output"))
                output = x.group("output")
                # remove space inside output:
                output = re.sub("\s", "", output)
                if len(output) > 0:
                    found_texts.append(x.group())
                    pred[field] = output
                    if text_inplace:
                        text = text.replace(x.group(), "", 1)
            return pred, text

        for field in fields:
            # print(field)
            # print(hint_dict[field]["type"])
            # print(hint_dict[field]["keyword"])
            regex = PostprocessRegex.regex_gen(keywords=hint_dict[field]["keyword"],
                                               output_type=hint_dict[field]["type"],
                                               keyword_need=True)
            pred, text = regex_search(regex, text, pred, field)

        # remove all texts found matched in the first search
        # run the search again, but without keyword

        # print("text after first search", text)

        # run the search again for field: "issue date", with keyword at the back of the output date
        regex = PostprocessRegex.regex_gen(keywords=hint_dict["IssueDate"]["keyword"],
                                           output_type=hint_dict["IssueDate"]["type"],
                                           keyword_position="back")
        pred, text = regex_search(regex, text, pred, "IssueDate")
        # print("text after IssueDate search", text)

        # run the search again, but without keyword
        # and only for the field that have not found matched value in the first search
        for field in fields:
            if pd.isna(pred[field]):
                # print(field)
                # print(hint_dict[field]["type"])
                # print(hint_dict[field]["keyword"])
                regex = PostprocessRegex.regex_gen(keywords=hint_dict[field]["keyword"],
                                                   output_type=hint_dict[field]["type"],
                                                   keyword_need=False)
                # print("regex", regex)
                pred, text = regex_search(regex, text, pred, field, text_inplace=True)

        return pred


# if __name__ == "__main__":
#     # ## load data
#     # ### neccessary files
#     # path the folder that contains images
#     image_dir = work_dir_path + "/data/raw/insurance_cards/image"
#
#     # path to labled dict
#     label_dict_dir = work_dir_path + "/data/processed/insurance_card/label/label_dict.pkl"
#
#     # path to gcv response dict
#     gcv_dict_dir = work_dir_path + "/data/processed/insurance_card/gcv_response/gcv_response.pkl"
#     # -
#
#     # ### load dictionaries
#
#     # gcv response dictionary
#     gcv_res_dict = pickle.load(open(gcv_dict_dir, 'rb'))
#
#     # label dictionary
#     label_dict = pickle.load(open(label_dict_dir, 'rb'))
#
#     # dictionary containing all of the longest text returned by GCV
#     gcv_text_dict = {k: gcv_res_dict[k]["final_text"] for k in list(gcv_res_dict.keys())}
#
#     # raw_text = gcv_res_dict["01"]["final_text"]
#     raw_text = gcv_text_dict["01"]
#     print("raw_text:", raw_text)
#
#     reformed_text = PostprocessRegex.gcv_text_reform(text=raw_text)
#     print("reformed_text", reformed_text)
#
#     keywords = hint_dict["Dependent"]["keyword"]
#     data_type = hint_dict["Dependent"]["type"]
#
#     print("keywords:", keywords)
#     print("data_type:", data_type)
#
#     print(PostprocessRegex.data_type_to_regex(data_type=data_type))
#     print(PostprocessRegex.regex_gen(keywords=keywords, output_type=data_type))
#
#     pred = PostprocessRegex.extract_info_regex(text=reformed_text)
#     pprint.pprint(pred)
#
#     # run for multiple images/keys
#     pred_multi = PostprocessRegex.extract_info_multi(input_dict=gcv_text_dict)
#     # pprint.pprint(pred_multi)
#
#     # compute accuracy
#     accuracy = Metric.metric(predict=pred_multi, target=label_dict)
#     pprint.pprint(accuracy, depth=3, width=100)
