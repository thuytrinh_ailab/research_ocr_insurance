import IPython
import pandas as pd
from matplotlib import pyplot as plt
import cv2


class Display:

    @staticmethod
    def display_dict_nb(dict, transpose=False):
        """
        display dictionary in Jupyter Notebook
        :param dict:
        :param transpose:
        :return: None
        """
        # convert dictionary to pandas table
        df = pd.DataFrame(dict)
        # transpose the dataframe if needed
        if transpose:
            df = df.T
        # print the table
        IPython.display.display(df)

    @staticmethod
    def display_img_nb(input_img, input_type="nparray"):
        """
        display images inside jupyter notebook
        :param input_img: could be path to the image or images in nparray
        :param input_type: format of input, either "image_path" or "nparray"
        :return:
        """
        if input_type == "image_path":
            img = cv2.imread(input_img, 1)
        else:
            img = input_img
        plt.imshow(img)
        plt.show()

    @staticmethod
    def draw_boxes(img, bps):
        """Draw a border around the image using the hints in the vector list."""

        img_copy = img.copy()
        if isinstance(bps, list):
            for bp in bps:
                (tl, tr, br, bl) = bp
                (x, y) = tl
                (w, h) = (br[0] - bl[0], bl[1] - tl[1])
                # react = (x, y, w, h)
                # # print(react)
                cv2.rectangle(img_copy, (x, y), (x + w, y + h), (0, 0, 255), 5)
        else:
            (tl, tr, br, bl) = bps
            (x, y) = tl
            (w, h) = (br[0] - bl[0], bl[1] - tl[1])
            # react = (x, y, w, h)
            # # print(react)
            cv2.rectangle(img_copy, (x, y), (x + w, y + h), (0, 0, 255), 5)

        Display.display_img_nb(img_copy)


