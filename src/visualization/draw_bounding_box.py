import argparse
from enum import Enum
import io
import os
from PIL import Image, ImageDraw
from src.config import GCV, PrjInsuranceConf
from google.cloud import vision
from google.cloud.vision import types

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = GCV.gcv_key_jsonfilepath
client = vision.ImageAnnotatorClient()

class FeatureType(Enum):
    PAGE = 1
    BLOCK = 2
    PARA = 3
    WORD = 4
    SYMBOL = 5

# Todo: refactor the draw box function from inputs of bound.vertices returned from GCV

def draw_boxes(image, bounds, color):
    """Draw a border around the image using the hints in the vector list."""
    draw = ImageDraw.Draw(image)

    for bound in bounds:
        draw.polygon([
            bound.vertices[0].x, bound.vertices[0].y,
            bound.vertices[1].x, bound.vertices[1].y,
            bound.vertices[2].x, bound.vertices[2].y,
            bound.vertices[3].x, bound.vertices[3].y], None, color)
    return image


def get_document_bounds(image_file, feature):
    # [START vision_document_text_tutorial_detect_bounds]
    """Returns document bounds given an image."""

    bounds = []

    with io.open(image_file, 'rb') as image_file:
        content = image_file.read()

    image = types.Image(content=content)

    response = client.document_text_detection(image=image)
    document = response.full_text_annotation

    # Collect specified feature bounds by enumerating all document features
    for page in document.pages:
        for block in page.blocks:
            for paragraph in block.paragraphs:
                for word in paragraph.words:
                    for symbol in word.symbols:
                        if (feature == FeatureType.SYMBOL):
                            bounds.append(symbol.bounding_box)

                    if (feature == FeatureType.WORD):
                        bounds.append(word.bounding_box)

                if (feature == FeatureType.PARA):
                    bounds.append(paragraph.bounding_box)

            if (feature == FeatureType.BLOCK):
                bounds.append(block.bounding_box)

        if (feature == FeatureType.PAGE):
            bounds.append(block.bounding_box)
    print(bounds, "\n")

    # The list `bounds` contains the coordinates of the bounding boxes.
    # [END vision_document_text_tutorial_detect_bounds]
    return bounds


def render_doc_text(filein):
    image = Image.open(filein)
    bounds = get_document_bounds(filein, FeatureType.PAGE)
    draw_boxes(image, bounds, 'blue')
    bounds = get_document_bounds(filein, FeatureType.PARA)
    draw_boxes(image, bounds, 'red')
    bounds = get_document_bounds(filein, FeatureType.WORD)
    draw_boxes(image, bounds, 'yellow')

    #     show_img(image, "text with bounding box")
    display(image)

# # this part is commented out since we don't want to save the output file, just visualise it is enough
#     if fileout is not 0:
#         image.save(fileout)
#     else:
#         image.show()


if __name__ == '__main__':
    # # [START vision_document_text_tutorial_run_application]
    # parser = argparse.ArgumentParser()
    # parser.add_argument('/Users/thuyttt/Documents/AI-Lab/05_Programming/ocr-research/data/raw/1_buy_sell.jpg', help='The image for text detection.')
    # # parser.add_argument('text_detected/', help='Optional output file', default=0)
    # args = parser.parse_args()

    file_in_path = PrjInsuranceConf.work_dir_path + "/data/raw/1_buy_sell.jpg"
    render_doc_text(file_in_path)