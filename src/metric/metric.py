from Levenshtein import distance


class Metric:

    @staticmethod
    def normalized_lev(predict, target):
        """
        This function is to calculate normalized Levenshtein distance for each pair of string

        param:
        pred -- predicted string
        tar -- target string

        return: normalized Levenshtein distance value, which equals Levenshtein distance divided by length of longer string.

        """

        return min(distance(predict, target) / len(target),1)

    @staticmethod
    def discrete_distance(predict, target):
        """
        This function is to calculate discrete distance

        param:
        pred -- a dictionary contains predicted values
        tar --  a dictionary contains target values

        return: 0 if 2 values equal and 1 otherwise

        """

        if predict == target:
            return 0
        else:
            return 1

    @staticmethod
    def LER(predict, target, field="all"):
        """
        This function is to calculate label error rate of prediction result for 1 single image

        param:
        predict -- a dictionary contains predicted values
        target --  a dictionary contains target values
        field -- name of the field that predicted value is compared to target value
                 default: "all" fields available in both predict and target dictionaries

        return: average label error rate of sequences

        """

        if field == "all":
            interested_fields = list(predict.keys())
        else:
            interested_fields = field

        # create result dictionary
        ler = {f: 1 for f in interested_fields}

        for field in interested_fields:
            pred_str = str(predict[field])
            tar_str = str(target[field])
            ler[field] = Metric.normalized_lev(predict=pred_str, target=tar_str)

        return ler

    @staticmethod
    def SER(predict, target, field="all"):
        """
        This function is to calculate label error rate of prediction result for 1 single image

        param:
        predict -- a dictionary contains predicted values
        target --  a dictionary contains target values
        method -- method of calculating distance between the predict and target strings,
                 only 2 methods implemented: lev(Levenshtein) and dis (Discrete)
        field -- name of the field that predicted value is compared to target value
                 default: "all" fields available in both predict and target dictionaries

        return: average label error rate of sequences

        """

        if field == "all":
            interested_fields = list(predict.keys())
        else:
            interested_fields = field

        # create result dictionary
        ser = {f: 1 for f in interested_fields}

        for field in interested_fields:
            pred_str = str(predict[field])
            tar_str = str(target[field])
            ser[field] = Metric.discrete_distance(predict=pred_str, target=tar_str)

        return ser

    @staticmethod
    def err_rate(predict, target, field="all"):
        """
        This function is to calculate label error rate of prediction result for 1 single image

        param:
        predict -- a dictionary contains predicted values
        target --  a dictionary contains target values
        field -- name of the field that predicted value is compared to target value
                 default: "all" fields available in both predict and target dictionaries

        return: error rates calculated using 2 methods for computing distance between the predict and target strings: lev(Levenshtein) and dis (Discrete)

        """

        if field == "all":
            interested_fields = list(predict.keys())
        else:
            interested_fields = field

        # create result dictionary
        err = {f: {"ler": 1, "discrete": 1} for f in interested_fields}

        for field in interested_fields:
            pred_str = str(predict[field])
            tar_str = str(target[field])
            err[field]["ler"] = Metric.normalized_lev(predict=pred_str, target=tar_str)
            err[field]["discrete"] = Metric.discrete_distance(predict=pred_str, target=tar_str)

        return err

    @staticmethod
    def metric(predict, target, key="all", field="all"):
        """
        compute accuracy metrics using both Levenshtein and Discrete distance

        :param predict: dictionary of predict result:
                        {@key: name of image,
                        @value: dictionary prediction with {@key: fields, @value: predicted string/text}}
        :param target: dictionary of label data:
                        {@key: name of image,
                        @value: dictionary prediction with {@key: fields, @value: correct text}}
        :param key: image name, can be a single string or vector of input name
                    default: "all" - all images contains in the predict dictionary
        :param field:
        :return:
        """
        if key == "all":
            interested_keys = list(predict.keys())
        else:
            interested_keys = key

        if field == "all":
            interested_fields = list(predict[interested_keys[0]].keys())
        else:
            interested_fields = field

        # create result dictionary
        acc_dict = {f: {"lev": 0, "discrete": 0} for f in interested_fields}

        for field in interested_fields:
            lev = 0
            ser = 0
            for key in interested_keys:
                pred_str = str(predict[key][field])
                tar_str = str(target[key][field])
                tmp_lev = Metric.normalized_lev(predict=pred_str, target=tar_str)
                tmp_ser = Metric.discrete_distance(predict=pred_str, target=tar_str)
                # if tmp_lev > 1:
                #     print("wrong lev rate:\n lev=", tmp_lev, "key=", key)
                # if tmp_ser > 1:
                #     print("wrong ser rate:\n lev=", tmp_ser, "key=", key)
                lev = lev + tmp_lev
                ser = ser + tmp_ser

            acc_dict[field]["lev"] = 1 - lev/len(interested_keys)
            acc_dict[field]["discrete"] = 1 - ser/len(interested_keys)

        # acc_df = pd.DataFrame(acc_dict)
        # acc_df = acc_df.T
        # IPython.display.display(acc_df)

        return acc_dict











