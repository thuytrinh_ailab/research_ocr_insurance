# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 1.0.5
#   kernelspec:
#     display_name: ocr_insurance
#     language: python
#     name: ocr_insurance
# ---

# # apply regex for text extraction

# # setup

# %load_ext autoreload
# %autoreload 2

# ## import packages

from src.config import work_dir_path
import re
from src.ocr_base.text_processing.normlise_text import Normaliser
import pickle  # library to save python dictionaries into a file that can be loaded again
from jupyter_helpers.namespace import NeatNamespace # package to summarise dictionaries into nice table view, syntax NeatNamespace(theDictionary)
import IPython
import pandas as pd
from src.ocr_base.text_processing.match_inline_texts import MatchLine 
from src.ocr_insurance_card.text_processing.regex import PostprocessRegex as PR

# ## load data

# ### neccessary files

# +
# path the folder that contains images
image_dir = work_dir_path + "/data/raw/insurance_cards/image"

# path to labled dict
label_dict_dir = work_dir_path + "/data/processed/insurance_card/label/label_dict.pkl"

# path to gcv response dict
gcv_dict_dir = work_dir_path + "/data/processed/insurance_card/gcv_response/gcv_response.pkl"
# -

# ### load dictionaries

# gcv response dictionary
gcv_res_dict = pickle.load(open(gcv_dict_dir, 'rb'))
NeatNamespace(gcv_res_dict["01"])

# label dictionary
label_dict = pickle.load(open(label_dict_dir, 'rb'))
NeatNamespace(label_dict["01"])

# # method

# ## regex with keyword
# simple rule that find keyword then take the string right after the keyword

# create keyword dictionary
keyword_dict = {"Dependent": ["本人", "家族"],
                "Symbol": "記号",
               "Number": "番号",
               "Name": "氏名",
               "NameKatakana": "フリガナ",
               "Sex": "性別",
               "Birthday": "生年月日",
               "AcquisitionDate": ["資格取得年月日", "認定年月日"],
               "IssueDate": "交付年月日",
               "InsuredPersonName": ["組合員氏名", "被保険者氏名", "世帯主氏名"],
               "ExpireDate": "有効期限",
               "Address": "住所",
               "WorkplaceName": "事業所名称",
               "WorkplaceAddress": "事業所所在地",
               "WorkplacePhoneNo": "事業所電話番号",
               "保険者番号": "保険者番号",
               "InsurerName": "保険者名称",
               "IssuerAddress": "保険者所在地",
               "IssuerPhoneNo": "保険者電話番号"}

keyword_dict["Number"]

interested_fields = ["Dependent", "Symbol", "Number", "Name", "NameKatakana", "Sex",
                     "Birthday", "AcquisitionDate", "IssueDate", "InsuredPersonName", "ExpireDate",
                     "Address", "WorkplaceName", "WorkplaceAddress", "WorkplacePhoneNo", "InsurerName",
                     "IssuerAddress", "IssuerPhoneNo"]

hint_dict = {n:{} for n in interested_fields}
for field in interested_fields:
    hint_dict[field]['keyword'] = keyword_dict[field]
NeatNamespace(hint_dict)

hint_dict = {"Dependent": {"keyword": ["本人", "家族"], "type": "word"},
             "Symbol": {"keyword": "記号", "type": "number"},
             "Number": {"keyword": "番号", "type": "number"},
             "Name": {"keyword": "氏名", "type": "person_name"},
             "NameKatakana": {"keyword": "フリガナ", "type": "word"},
             "Sex": {"keyword": "性別", "type": "word"},
             "Birthday": {"keyword": "生年月日", "type": "date"},
             "AcquisitionDate": {"keyword": ["資格取得年月日", "認定年月日"], "type": "date"},
             "IssueDate": {"keyword": "交付年月日", "type": "date"} ,
             "InsuredPersonName": {"keyword": ["組合員氏名", "被保険者氏名", "世帯主氏名"], "type": "person_name"},
             "ExpireDate": {"keyword": "有効期限", "type": "date"},
             "Address": {"keyword": "住所", "type": "address"},
             "WorkplaceName": {"keyword": "事業所名称", "type": "word"},
             "WorkplaceAddress": {"keyword": "事業所所在地", "type": "address"},
             "WorkplacePhoneNo": {"keyword": "事業所電話番号", "type": "phone_no"},
             "保険者番号": {"keyword": "保険者番号", "type": "number"},
             "InsurerName": {"keyword": "保険者名称", "type": "word"},
             "IssuerAddress": {"keyword": "保険者所在地", "type": "address"},
             "IssuerPhoneNo": {"keyword": "保険者電話番号", "type": "phone_no"}}
NeatNamespace(hint_dict)

hint_dict["Dependent"]["type"]

# ### view reponse text

final_text = gcv_res_dict["01"]["final_text"]
final_text

full_text = gcv_res_dict["01"]["full_text"]
for text in full_text[1:]:
    print(text)

# separate text by "\n"
input_text = final_text
texts_sep = input_text.split("\n")
texts_sep

# +
# drop empty element
for string in texts_sep:
    if len(string) == 0:
        texts_sep.remove(string)
        
texts_sep
# -

# normalise the text
for text in texts_sep:
    idx = texts_sep.index(text)
    norm_text = Normaliser.unicode_normalize('０-９Ａ-Ｚａ-ｚ｡-ﾟ', text)
    texts_sep[idx] = norm_text
texts_sep

NeatNamespace(label_dict["01"])


# def gcv_text_reform(text):
#     # separate text by "\n"
#     texts_sep = text.split("\n")
#
#     for string in texts_sep:
#         # normalise the text
#         idx = texts_sep.index(string)
#         norm_text = Normaliser.unicode_normalize('０-９Ａ-Ｚａ-ｚ｡-ﾟ', string)
#         texts_sep[idx] = norm_text
#
#         # drop empty element
#         if len(string) == 0:
#             texts_sep.remove(string)
#     return texts_sep
#
#
# gcv_text_reform(gcv_res_dict["01"]["final_text"])


def gcv_text_reform(text):
    # separate text by "\n"
    texts_sep = text.split("\n")
    
    all_text = ""
    for string in texts_sep:
        # normalise the text
        idx = texts_sep.index(string)
        norm_text = Normaliser.unicode_normalize('０-９Ａ-Ｚａ-ｚ｡-ﾟ', string)
        texts_sep[idx] = norm_text
        
        # drop empty element
        if len(string) == 0:
            texts_sep.remove(string)
        else:
            all_text = all_text + " " + norm_text
            
    return all_text


texts = gcv_text_reform(gcv_res_dict["01"]["final_text"])


# ### apply regex

# 
def data_type_to_regex(data_type):
    """
    generate regex for different data type
    return any Alphanumeric characters  as default value if none of the pre-defined type matched
    
    @para
    data_type: a string that defines type of data
    
    @retun
    corresponding regex 
    """    
    switcher = {
        "number": "\d{3,}",
        "date": "\w{2}?\d{2}年\s?\d{1,2}月\d{1,2}日",
        "phone_no": "\d{2,3}-\d{3,4}-\d{4}",
        "person_name": "\w{1,3}\s\w{1,3}",
        "address": "(\w*)(町\d+-\d+-\d+)"
    }
    return switcher.get(data_type, "\w*")


data_type_to_regex("word")


def regex_gen(keyword, output_type, keyword_need=True):
    """
    generating regex from keyword and output_type
    simple logic: keyword + space + regex of output_type
    
    @para
    keyword: string of keyword
    output_type: a string that defines output type
    
    @return
    corresponding regex
    """
    output_type_regex = data_type_to_regex(output_type)
    if keyword_need:
        pattern = "(?P<keyword>" + keyword + ")" + "(" + "\s*" + ")" + "(?P<output>" + output_type_regex + ")"
    else:
        pattern = "(?P<keyword>" + keyword + ")?" + "(" + "\s*" + ")" + "(?P<output>" + output_type_regex + ")"
    regex = re.compile("{}".format(pattern))
    return regex


regex_gen(hint_dict["Number"]["keyword"], hint_dict["Number"]["type"])

pred = {i: {n: None for n in interested_fields} for i in gcv_res_dict}
NeatNamespace(pred["01"])

regex = re.compile('(?P<keyword>保険者名称)(\\s*)(?P<output>\\w*)')
regex

regex = regex_gen(hint_dict["Number"]["keyword"], hint_dict["Number"]["type"])
regex

texts_sep = MatchLine.match_inline(gcv_res_dict["01"])
for text in texts_sep:
    print("text", text)
    x = re.search(regex, text)
    if (x):
        print("found match!\n matched text:", text)
        print("output:", x.group("output"))
        print("group:", x.group())
    else:
        print("not found")

all_texts = PR.gcv_text_reform(reformed_texts)
print("all_texts", all_texts)
extract_info_regex(text=all_texts, hint_dict=hint_dict, fields=interested_fields)


def extract_info_regex(text, hint_dict, fields):
    """
    extract information using regex of keyword and data type
    apply for one single image only

    :param text: a single string of text from which we need to extract information from
    :param hint_dict: dictionary containing 2 type of hints about the field: `keyword` and `type`
    :param fields: a vectors of name of the fields that need to extract information
    :return:
    pred: a dictionary contains all predicting output
    """
    pred = {f: None for f in fields}
    # search the one time for regex with keyword and type
    found_texts = []
    for field in fields:
        print(field)
        print(hint_dict[field]["type"])
        print(hint_dict[field]["keyword"])
        regex = PR.regex_gen(keywords=hint_dict[field]["keyword"],
                                           output_type=hint_dict[field]["type"],
                                           keyword_need=True)
        print("regex", regex)
        x = re.search(regex, text)
        if x:
            print("found match in 1st search!\n matched text:", text)
            print("output:", x.group("output"))
            output = x.group("output")
            # remove space inside output:
            output = re.sub("\s", "", output)
            if len(output) > 0:
                found_texts.append(x.group())
                pred[field] = output
                
    # remove all texts found matched in the first search
    # run the search again, but without keyword

    # remove all the found text
    for str_value in found_texts:
        text.replace(str_value, "", 1)

    # run the search again, but without keyword
    # and only for the field that have not found matched value in the first search
    for field in fields:
        if pred[field] == None:
            regex = PR.regex_gen(keywords=hint_dict[field]["keyword"],
                                               output_type=hint_dict[field]["type"],
                                               keyword_need=False)
            x = re.search(regex, text)
            if x:
                print("found match in 2nd search!\n matched text:", text)
                print("output:", x.group("output"))
                output = x.group("output")
                # remove space inside output:
                output = re.sub("\s", "", output)
                if len(output) > 0:
                    pred[field] = output

    return pred


reformed_texts = MatchLine.match_inline(gcv_res_dict["01"])
all_texts = PR.gcv_text_reform(reformed_texts)
print("all_texts", all_texts)
for field in pred["01"]:
    if field == "IssuerPhoneNo":
            keyword_need = False
    else:
        keyword_need = True
    print(field)
    print(hint_dict[field]["type"])
    print(hint_dict[field]["keyword"])
    if isinstance(hint_dict[field]["keyword"], list):
        keyword = hint_dict[field]["keyword"][0]
        for key in hint_dict[field]["keyword"][1:]:
            keyword = keyword + "|" + key
        regex = regex_gen(keyword, hint_dict[field]["type"], keyword_need)
    else:
        regex = regex_gen(hint_dict[field]["keyword"], hint_dict[field]["type"], keyword_need)
#         regex = re.compile(pattern)
    print(regex)
    x = re.search(regex, all_texts)
    if (x):
        output = x.group("output")
        # remove space inside output:
        output = re.sub("\s", "", output)
        if pred["01"][field] == None:
            pred["01"][field] = output
        else:
            if isinstance(pred["01"][field], str):
                pred["01"][field] = []
                pred["01"][field].append(output)
        print("found match!\n matched text:", text)
        print("output:", x.group("output"))
    else:
        print("not found")

NeatNamespace(pred["01"])

# +
pred = {i: {n: None for n in interested_fields} for i in gcv_res_dict}

for image in pred:
#     print(image)
    detected_text = gcv_res_dict[image]["final_text"]
#     texts_sep = gcv_text_reform(detected_text)
    all_text = gcv_text_reform(detected_text)
    
    for field in pred[image]:
        if field == "IssuerPhoneNo":
            keyword_need = False
        else:
            keyword_need = True
#         print(field)
#         print(hint_dict[field]["type"])
#         print(hint_dict[field]["keyword"])
        if isinstance(hint_dict[field]["keyword"], list):
            keyword = hint_dict[field]["keyword"][0]
            for key in hint_dict[field]["keyword"][1:]:
                keyword = keyword + "|" + key
            regex = regex_gen(keyword, hint_dict[field]["type"], keyword_need)
        else:
            regex = regex_gen(hint_dict[field]["keyword"], hint_dict[field]["type"], keyword_need)
#         for text in texts_sep:
#             x = re.search(regex, text)
#             if (x):
#                 output = x.group("output")
#                 # remove space inside output:
#                 output = re.sub("\s", "", output)
#                 if pred[image][field] == None:
#                     pred[image][field] = output
#                 else:
#                     if isinstance(pred[image][field], str):
#                         pred[image][field] = []
#                     pred[image][field].append(output)
#                 print("found match!\n matched text:", text)
#                 print("output:", x.group("output"))
        x = re.search(regex, all_text)
        if (x):
            output = x.group("output")
            # remove space inside output:
            output = re.sub("\s", "", output)
            if pred[image][field] == None:
                pred[image][field] = output
            else:
                if isinstance(pred[image][field], str):
                    pred[image][field] = []
                pred[image][field].append(output)
# -

NeatNamespace(pred["01"])

# The simple Keyword + WordType rule seems to only work for `Symbol`, `Number`, `Birthday`, `AcquisitionDate`, `Sex` and `Name` 
#
# We can try compute accuracy for those fields now

# ### Result

from src.ocr_base.accuracy.metric import Metric

key = "115"
NeatNamespace(pred[key])

NeatNamespace(label_dict[key])

from src.ocr_base.display.display import Display
err = Metric.err_rate(predict=pred[key], target=label_dict[key])
Display.display_dict_nb(err, transpose=True)

from src.ocr_base.accuracy.metric import Metric
acc_dict = Metric.metric(predict=pred, target=label_dict)
Display.display_dict_nb(acc_dict, transpose=True)

# +
# save the result
out_path = work_dir_path + "/reports/2019_04_17/results"
# to_csv
acc_df = pd.DataFrame(acc_dict)
acc_df.to_csv(out_path+"/accuracy.csv", header=True)

# as dictionary
f = open(out_path+"/accuracy.pkl","wb")
pickle.dump(acc_dict,f)
f.close()
# -

# ## concat words from the same line

# We can try to concat words from the same line into one word, using their bounding box position, before passing it to the keyword/type regex

texts = gcv_res_dict["01"]["full_text"][0]
print('\n"{}"'.format(texts))

gcv_res_dict["01"]["bouding_poly"][0]

# convert bouding poly string into int
string = gcv_res_dict["01"]["bouding_poly"][0]
string

pattern = "(?P<keyword>" + keyword + ")" + "(" + "\s*" + ")" + "(?P<output>" + output_type_regex + ")"

regex = r'[<(?P<tl>\(\d{1,},\d{1,}\)),(?P<tr>\(\d{1,},\d{1,}\)),(?P<br>\(\d{1,},\d{1,}\)),(?P<bl>\(\d{1,},\d{1,}\))]'

# +
import io
import os
from src.config import gcv_key_jsonfilepath, gcv_language

def detect_text(path):
    """Detects text in the file."""
    from google.cloud import vision
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = gcv_key_jsonfilepath
    client = vision.ImageAnnotatorClient()

    with io.open(path, 'rb') as image_file:
        content = image_file.read()
    # Language hint codes for OCR
    image_context = vision.types.ImageContext(
        language_hints=gcv_language)

    image = vision.types.Image(content=content)

    response = client.text_detection(image=image,
                                    image_context=image_context)
    
    #text_detection_response = vision_client.text_detection({
    #    'source': {'image_uri': 'gs://{}/{}'.format(bucket, filename)}
    #})
    
    texts = response.text_annotations
    vertices = (['({:d},{:d})'.format(vertex.x, vertex.y)
                    for vertex in texts[0].bounding_poly.vertices])
    four_points = '[{}]'.format(','.join(vertices))
    print('\n"{}"'.format(texts[0].description))
    
    return four_points
print(image_dir + "/01.jpg")
fpts = detect_text(image_dir + "/01.jpg")
# -

from src.ocr_base.image_processing.bounding_poly import BoudingPoly
import numpy as np
# pts = gcv_res_dict["01"]["bouding_poly"][1]
fpts_int = np.array(eval(fpts), dtype = "float32")
print(fpts_int)

text_sep = gcv_res_dict["01"]["full_text"][1:]
bp = gcv_res_dict["01"]["bouding_poly"][1:]

# convert bp into numpy array
bp_array = [np.array(eval(element), dtype = "float32") for element in bp]
# matching text by comparing the bouding poly position
text_line_match = text_sep[0]
for idx in range(1,len(text_sep)):
    text = text_sep[idx]
    tmp_bp = bp_array[idx]
    print(text)
    (tl, tr, br, bl) = tmp_bp
    print(tl, tr, br, bl)
    # we will compare the 3 lines:
    # 1.bottom line, 2.middle line: the line between the bottom and top line of the rectangular
    # if any of the 2 line 


