from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='research OCR technique to read and extract information from images',
    author='thuyttt_mti',
    license='',
)
